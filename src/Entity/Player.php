<?php
namespace App\Entity;

class Player
{
    private const PLAY_PLAY_STATUS = 'play';
    private const BENCH_PLAY_STATUS = 'bench';

    private int $number;
    private string $name;
    private string $playStatus;
    private int $inMinute;
    private int $outMinute;
    private int $addedMinute = 0;
    private array $goalsMinute;
    private int $yellowCard;
    private int $redCard;
    private string $position;

    public function __construct(int $number, string $name, string $position)
    {
        $this->number = $number;
        $this->name = $name;
        $this->playStatus = self::BENCH_PLAY_STATUS;
        $this->inMinute = 0;
        $this->outMinute = 0;
        $this->goalsMinute = [];
        $this->yellowCard = 0;
        $this->redCard = 0;
        $this->position = $position;
    }

    public function getNumber(): int
    {
        return $this->number;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getInMinute(): int
    {
        return $this->inMinute;
    }

    public function getOutMinute(): int
    {
        return $this->outMinute;
    }

    public function isPlay(): bool
    {
        return $this->playStatus === self::PLAY_PLAY_STATUS;
    }

    public function getPlayTime(): int
    {
        if(!$this->outMinute) {
            return 0;
        }

        return ($this->outMinute - $this->inMinute) + $this->addedMinute;
    }

    public function goToPlay(int $minute): void
    {
        $this->inMinute = $minute;
        $this->playStatus = self::PLAY_PLAY_STATUS;
    }

    public function addAddedMinute(int $minute) {
        $this -> addedMinute = $minute;
    }

    public function addGoal(int $minute): void
    {
        $this -> goalsMinute[] = $minute;
    }

    public function getScoreGoal(): array
    {
        return $this->goalsMinute;
    }

    public function addYellowCard(int $minute): void
    {
        if ($this->yellowCard !== 0) $this->addRedCard($minute);
        else $this->yellowCard = $minute;
    }

    public function getYellowCards(): int
    {
        return $this->yellowCard;
    }

    public function addRedCard(int $minute): void
    {
        $this->redCard = $minute;
    }

    public function getRedCards(): int
    {
        return $this->redCard;
    }

    public function getPosition(): string
    {
        return $this->position;
    }

    public function goToBench(int $minute): void
    {
        $this->outMinute = $minute;
        $this->playStatus = self::BENCH_PLAY_STATUS;
    }
}